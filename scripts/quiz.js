// Code Credits
// Israel Aristide 100%

let intelCPU = document.getElementById("radio-intel");
let amdCPU = document.getElementById("radio-amd");

let amdGPU = document.getElementById("radio-amd-gpu");
let intelGPU = document.getElementById("radio-intel-gpu");
let intelGPULabel = document.getElementById("radio-intel-gpu-label")
let nvGPU = document.getElementById("radio-nv");

let intelField = document.getElementById("intel-cpu-field");
let amdField = document.getElementById("amd-cpu-field");
let amdGField = document.getElementById("amd-gpu-field");
let intelGField = document.getElementById("intel-gpu-field");
let nvField = document.getElementById("nv-gpu-field");

let button = document.getElementById("check-compat");

let formgpu = document.getElementById("form-gpu");
let formcpu = document.getElementById("form-cpu");
let formram = document.getElementById("form-ram");

let audio = document.getElementById('audio');


function intelCPUFunc() {

    intelField.style.display = "flex";
    amdField.style.display = "none";
    intelGPU.style.display = "inline";
    intelGPULabel.style.display = "inline";
}
function amdCPUFunc() {

    intelField.style.display = "none";
    amdField.style.display = "flex";
    intelGPU.style.display = "none";
    intelGPULabel.style.display = "none";
    intelGPU.checked = false;
    intelGField.style.display = "none";

}

function nvGPUFunc() {
    amdGField.style.display ="none";
    intelGField.style.display = "none";
    nvField.style.display = "flex";
}
function amdGPUFunc() {
    amdGField.style.display ="flex";
    intelGField.style.display = "none";
    nvField.style.display = "none";
}
function intelGPUFunc() {
    amdGField.style.display ="none";
    intelGField.style.display = "flex";
    nvField.style.display = "none";
}

if (intelCPU.checked) {
    intelCPUFunc();
} else if (amdCPU.checked) {
    amdCPUFunc();
}


if (nvGPU.checked) {
    nvGPUFunc();
} else if (amdGPU.checked) {
    amdGPUFunc();
} else if (intelGPU.checked) {
    intelGPUFunc();
}

console.log("Adding event listeners");

intelCPU.addEventListener("click", intelCPUFunc);
amdCPU.addEventListener("click", amdCPUFunc);

amdGPU.addEventListener("click", amdGPUFunc);
nvGPU.addEventListener("click", nvGPUFunc);
intelGPU.addEventListener("click", intelGPUFunc);

function compatCheck() {

    let cpu = document.querySelector("input[name='cpu-brand']:checked").value;
    let gpu = document.querySelector("input[name='gpu-brand']:checked").value;
    let cpuvalue = null;
    let gpuvalue = null;
    let result = true;


    if (cpu === 'intel') {
        let cpudetail = document.getElementById("cpu-details-intel");
        cpuvalue = cpudetail.options[cpudetail.selectedIndex].value;

    } else if (cpu === 'amd') {
        let cpudetail = document.getElementById("cpu-details-amd");
        cpuvalue = cpudetail.options[cpudetail.selectedIndex].value;

    }

    if (gpu === 'nvidia') {
        let gpudetail = document.getElementById("gpu-details-nv");
        gpuvalue = gpudetail.options[gpudetail.selectedIndex].value;

    } else if (gpu === 'intel') {
        let gpudetail = document.getElementById("gpu-details-intel");
        gpuvalue = gpudetail.options[gpudetail.selectedIndex].value;

    } else if (gpu === 'amd') {
        let gpudetail = document.getElementById("gpu-details-amd");
        gpuvalue = gpudetail.options[gpudetail.selectedIndex].value;

    }

    if (gpuvalue === 'fermi' || gpuvalue === 'GCN3' || gpuvalue === '-') {
        result = false;
    }

    if (cpuvalue === 'kbLake' || cpuvalue === 'zen' || gpuvalue === '-') {
        result = false;
    }
    let ram = document.getElementById("ram-ammount");

    if (ram.value <= 4) {
        result = false;
    }


    if (result) {
        audio.play();
        alert("Your system is compatible!");
    } else {
        audio.play();
        alert("your system is incompatible.");
    }


}

function CPUcontinue() {
    formcpu.style.display = 'none';
    formgpu.style.display = 'block';
}

function GPUcontinue() {
    formgpu.style.display = 'none';
    formram.style.display = 'block';
}

button.addEventListener("mousedown", compatCheck);