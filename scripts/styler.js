// Code Credits
// 90% Yu Hua Yang
// 10% Israel Aristide

// Script for Site-Wide Dark and light mode

// This ensures that the dark mode setting is saved throughout the pages
if (localStorage.getItem("appearance") === "dark") {
    setAppearance('dark')
} else {
    setAppearance('light')
}

// This function is for the toggle button itself
function toggleDarkmode() {
    if (document.body.id === "light-mode") {
        setAppearance('dark')
    } else {
        setAppearance('light')
    }
}

// sets the site appearance
function setAppearance(mode) {
    document.body.id = `${mode}-mode`
    localStorage.setItem("appearance", mode);


}